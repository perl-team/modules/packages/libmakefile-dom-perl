libmakefile-dom-perl (0.008-3) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 16:15:43 +0100

libmakefile-dom-perl (0.008-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Fix autopkgtest. Add build dependency on libtest-base-perl. During
    build, Test::Base is taken from inc/; for autopkgtest it seems safer
    to install the build dependency than to copy all of inc/ into the
    testbed.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.0.0.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Aug 2017 11:20:56 -0400

libmakefile-dom-perl (0.008-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Mikhail Gusarov ]
  * Remove myself from Uploaders.

  [ gregor herrmann ]
  * Add debian/upstream/metadata

  * Import upstream version 0.008
    Fixes "Possible precedence issue with control flow operator at
    inc/Test/Builder.pm"
    (Closes: #760746)
    Fixes "FTBFS with libtest-pod-coverage-perl: test failure"
    (Closes: #760748)
  * Refresh debian/patches/spelling.patch.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 May 2015 17:22:42 +0200

libmakefile-dom-perl (0.006-1) unstable; urgency=low

  * New upstream release.
  * Remove fix-pod-errors.patch, applied upstream.
  * Refresh spelling.patch, not applied upstream, applies with a fuzz.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Aug 2011 22:20:36 +0200

libmakefile-dom-perl (0.005-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update upstream information, bump DEP5 version and
    adjust formatting.
  * Set Standards-Version to 3.9.2 (no further changes).
  * Bump debhelper compatibility level to 8.
  * Refresh pod patch and add more DEP3 headers.
  * Add /me to Uploaders.
  * Add a new patch to fix some spelling mistakes.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Aug 2011 17:46:29 +0200

libmakefile-dom-perl (0.004-1) unstable; urgency=low

  * Initial release (Closes: #542913).

 -- Mikhail Gusarov <dottedmag@dottedmag.net>  Sun, 23 Aug 2009 05:44:18 +0700
